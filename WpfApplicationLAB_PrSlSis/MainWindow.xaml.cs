﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;
using System.Threading;

namespace SwitchAllColor
{
    public partial class MainWindow : Window
    {
        private delegate void paintDelegate();
        private delegate void proc(Color color);    
        private Color color = new Color();
        private List<byte> _forvardValues = new List<byte>();
        private List<byte> _reverseValues = new List<byte>();
        private bool _IsWorking;
        
        public MainWindow()
        {
            InitializeComponent();
            _refreshGrid(new Color() { A = 255, R = 0, B = 0, G = 0 });
            for (int i = 0; i <= 255; i++)
            {
                _forvardValues.Add((byte)i);
                _reverseValues.Add((byte)i);
            }            
            _reverseValues.Reverse();
        }

        private void _button_Click(object sender, RoutedEventArgs e)
        {
            if (!_IsWorking)
            {
                _IsWorking = true;
                Thread t = new Thread(Paint);
                t.IsBackground = true;
                t.Start();
            }                                               
        }
                
        private void Paint()
        {           
            color.A = 255;
            _makeOneColorTransition(_forvardValues, "G");                  
            _makeOneColorTransition(_forvardValues, "B");
            _makeOneColorTransition(_reverseValues, "G");
            _makeOneColorTransition(_forvardValues, "R");
            _makeOneColorTransition(_reverseValues, "B");
            _makeOneColorTransition(_forvardValues, "G");
            _makeOneColorTransition(_forvardValues, "B");
            _makeOneColorTransition(_reverseValues, "A");
            _IsWorking = false;
        }

        private void _refreshGrid(Color color)                
        {
            grid.Background = new SolidColorBrush(color);    
        }

        private void _makeOneColorTransition(List<byte> values, string baseColor)
        {            
            values.ForEach(value =>
            {
                switch (baseColor)
                {
                    case "R": color.R = value; break;
                    case "G": color.G = value; break;
                    case "B": color.B = value; break;
                    case "A": color.R = value;
                              color.G = value;
                              color.B = value; break;
                }                
                Delegate d = new proc(_refreshGrid);                
                Dispatcher.Invoke(d, new object[] { color });
                Thread.Sleep(4);
            });            
        }
    }
}
